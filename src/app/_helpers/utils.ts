import { Injectable } from '@angular/core';

import { API_URL } from '../configuration';

/**
 * Clase de ayuda para la realización de llamadas HTTP.
 */
export class Helper {
    /**
     * Completa la URL a llamar con la URL base.
     * @param fragment Fragmento de URL.
     */
    static getUrl(fragment: string) {
        return API_URL + fragment;
    }


}
