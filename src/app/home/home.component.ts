import { Component, OnInit } from '@angular/core';
import { CheckoutResponse } from '../_models/checkoutResponse';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  step: number;
  response: CheckoutResponse;
  constructor() { }

  ngOnInit() {
    this.step = 1;
  }

}
