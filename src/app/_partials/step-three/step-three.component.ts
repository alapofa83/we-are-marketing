import { Component, OnInit, AfterViewInit, AfterContentInit, AfterViewChecked, Input } from '@angular/core';
import { CheckoutResponse } from 'src/app/_models/checkoutResponse';


@Component({
  selector: 'app-step-three',
  templateUrl: './step-three.component.html',
  styleUrls: ['./step-three.component.css']
})
export class StepThreeComponent implements OnInit {
  @Input() response: CheckoutResponse =  new CheckoutResponse();
  constructor() { }

  ngOnInit() {
  }



}
