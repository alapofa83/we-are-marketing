import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  /**
   * if menu is visible
   */
  isVisible =  false;
  constructor() { }

  ngOnInit() {
    window.addEventListener('scroll', this.scroll, true);
  }

  change(event) {
    this.isVisible ? this.isVisible = false : this.isVisible = true;
  }

  /**
   * fix if user scrolls menu hides
   */
  scroll = (event): void => {
    this.isVisible = false;
  }

}
