import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Person } from '../../_models/person';

@Component({
  selector: 'app-step-one',
  templateUrl: './step-one.component.html',
  styleUrls: ['./step-one.component.css']
})
export class StepOneComponent implements OnInit {
  /**
   * emiter of step
   */
  @Output() changeStep: EventEmitter<number> = new EventEmitter();
  /**
    * access to form control
    */
  @ViewChild('formcustomerData') formcustomerData: NgForm;
  /**
   * person data
   */
  person: Person = new Person();

  constructor() { }

  ngOnInit() {
  }

  /**
   * emits step 2 number if form validation is correct
   */
  goToNextStep() {
    if (this.formcustomerData.valid) {
      this.changeStep.emit(2);
    }
  }
}
