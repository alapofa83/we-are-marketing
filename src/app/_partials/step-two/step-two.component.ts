import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ApiCallService } from 'src/app/services/api-call.service';
import { CheckoutResponse } from 'src/app/_models/checkoutResponse';

@Component({
  selector: 'app-step-two',
  templateUrl: './step-two.component.html',
  styleUrls: ['./step-two.component.css']
})
export class StepTwoComponent implements OnInit {
  /**
   * if it's loading
   */
  loading = false;
  /**
   * if there's and error
   */
  errorResponse = '';
  @Output() changeStep: EventEmitter<number> = new EventEmitter();
  @Output() response: EventEmitter<CheckoutResponse> = new EventEmitter();
  constructor(private apiCallService: ApiCallService) { }

  ngOnInit() {
  }

  goToNextStep() {
    this.loading = true;
    this.apiCallService.checkout('5e3d41272d00003f7ed95c09').subscribe((data: CheckoutResponse) => {
      // tarda muy poco la consulta para que se vea el loader agregue un timer
      setTimeout(() => {
        this.response.emit(data);
        this.changeStep.emit(3);
      }, 3000);
    }, error => {
      setTimeout(() => {
        this.errorResponse = error.message;
      }, 3000);
    });
  }

  goPreviousStep() {
    this.changeStep.emit(1);
  }
}
