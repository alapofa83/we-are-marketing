import { Component, OnInit } from '@angular/core';
import { ApiCallService } from './services/api-call.service';
import { CheckoutResponse } from './_models/checkoutResponse';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'we-are-marketing';
  id = '5e3d41272d00003f7ed95c09';
  constructor(private apiCallService: ApiCallService) {}
  ngOnInit() {

  }

  finishCheckout() {
    this.apiCallService.checkout(this.id).subscribe((data: CheckoutResponse) => {
      console.log(data);
    });
  }
}
