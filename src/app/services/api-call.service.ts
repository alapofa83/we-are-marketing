import { Injectable } from '@angular/core';
import { Helper } from '../_helpers/utils';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { CheckoutResponse } from '../_models/checkoutResponse';
import { throwError } from 'rxjs';

/**
 * Servicio para la gestión de datos de cursos.
 */
@Injectable()
export class ApiCallService {

  constructor(private httpClient: HttpClient) {}

  /**
   * api call for checkout
   * @params id string current checkout process
   * @returns observable of type CheckoutResponse
   */
  checkout(id: string): Observable<CheckoutResponse> {
    return this.httpClient.get<CheckoutResponse>(Helper.getUrl(id));
  }

}
